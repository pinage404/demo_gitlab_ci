def hello(name: str | None = "world") -> str:
    return f"Hello {name}"


def test_hello_world():
    assert hello() == "Hello world"


def test_hello_foo():
    assert hello("foo") == "Hello foo"
